import io
from pathlib import Path

from flask import Flask, request, send_file
from PIL import Image, ImageFont, ImageDraw


app = Flask(__name__)


@app.route("/")
def receipt():
    blank_receipt_path = Path(__file__).parent / 'uplatnica.png'

    i = Image.open(blank_receipt_path)
    draw = ImageDraw.Draw(i)

    font = 'arial.ttf'
    font_small = ImageFont.truetype(font, 18)
    font_big = ImageFont.truetype(font, 28)

    def q(query_param):
        return request.args.get(query_param, '')

    draw.text(
        (65, 65),
        '{}\n{}\n{}'.format(q('uplatilac'), q('adresa_uplatioca'), q('grad_uplatioca')),
        (0, 0, 0), font=font_small,

    )
    draw.text(
        (65, 190),
        q('svrha'),
        (0, 0, 0), font=font_small, size=150,
    )
    draw.text(
        (65, 315),
        '{}\n{}\n{}'.format(q('primalac'), q('adresa_primaoca'), q('grad_primaoca')),
        (0, 0, 0), font=font_small,
    )

    draw.text(
        (695, 95),
        q('sifra'),
        (0, 0, 0), font=font_big,
    )
    draw.text(
        (800, 95),
        q('valuta'),
        (0, 0, 0), font=font_big,
    )
    draw.text(
        (920, 95),
        q('iznos'),
        (0, 0, 0), font=font_big,
    )
    draw.text(
        (800, 165),
        q('racun'),
        (0, 0, 0), font=font_big,
    )
    draw.text(
        (695, 240),
        q('model'),
        (0, 0, 0), font=font_big,
    )
    draw.text(
        (800, 238),
        q('pnb'),
        (0, 0, 0), font=font_big,
    )

    result = io.BytesIO()
    i.save(result, 'PNG')
    result.seek(0)

    return send_file(result, mimetype='image/png')
